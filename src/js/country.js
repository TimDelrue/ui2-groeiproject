class Country {
    constructor(id, name, population, democratic, currencyId) {
        this.id = id;
        this.name = name;
        this.population = population;
        this.democratic = democratic;
        this.currencyId = currencyId;
    }
}

const getCountryInstance = (obj) => {
    return new Country(obj.id, obj.name, obj.population, Boolean(obj.democratic), obj.currencyId);
}

export {getCountryInstance}