import 'bootstrap' // importeer bootstrap JavaScript code
import 'bootstrap-icons/font/bootstrap-icons.css'
import './css/style.scss'
import {loadDetails, loadSettings, loadTable, refreshCards, refreshForm} from "./js/refreshFunctions.js";
import {addEventListeners} from "./js/eventListeners.js";
import {startDB} from "./js/idb.js";

const init = () => {
    startDB().then(loadSettings)
    refreshForm()
    refreshCards()
    addEventListeners()
    loadTable()
    loadDetails()
}

init();