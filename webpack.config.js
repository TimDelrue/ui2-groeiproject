import HtmlWebpackPlugin from "html-webpack-plugin"
import MiniCssExtractPlugin from "mini-css-extract-plugin"


import  path from "path";
const config = {
    resolve: {
        fallback: {
            "fs": false,
            "tls": false,
            "net": false,
            "path": false,
            "zlib": false,
            "http": false,
            "https": false,
            "stream": false,
            "crypto": false,
            "console": false,
            "url": false,
            "os": false,
            "tty": false
        }
    },
    devtool: "source-map",
    output: {
        clean:true
    },
    mode: "development",
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './src/html/index.ejs'
        }),
        new MiniCssExtractPlugin()
    ],
    module: {
        rules: [
            {test: /\.css$/i,
                use: [MiniCssExtractPlugin.loader, "css-loader"]},
            // assets referred from HTML
            {
                test: /\.ejs?$/i,
                use: ['html-loader', 'template-ejs-loader']
            },

            // Image assets
            {
                test: /\.(png|svg|jpe?g|gif)$/i,
                type: "asset"
            },
            // Font assets
            {
                test: /\.(woff2?|eot|ttf|otf)$/i,
                type: "asset"
            },
            {
                test: /\.scss$/i,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            },
        ]
    },
    devServer: {
        static: { directory: path.resolve( "dist")},
        open: true,
        hot: false,
    }
}
export default config