const ValutaCardGrid = document.querySelector('.CardContainer');

export function CreateCardGrid() {
}

function createElement(type, parent, classList) {
    const element = document.createElement(type);
    if (classList !== undefined) {
        typeof classList === 'Array' && classList.length > 0
            ? element.classList.add(...classList)
            : (element.className = classList);
    }
    parent.append(element);
    return element;
}

export function createCard(valuta) {
    let cardDiv = createElement("div", ValutaCardGrid, "card mb-3");
    cardDiv.style.width = "40rem";
    let rowDiv = createElement("div", cardDiv, "row g-0");
    let col1 = createElement("div", rowDiv, "col-md-4");
    let img = createElement("img", col1, "img-fluid rounded-start");
    img.src = valuta.imageURL;
    img.alt = "Photo of " + valuta.name;
    let col2 = createElement("div", rowDiv, "col-md-8");
    let cardBody = createElement("div", col2, "card-body");
    let title = createElement("h5", cardBody, "card-title");
    title.innerHTML = valuta.name;
    let table = createElement("table", cardBody, "table table-light table-striped-columns");
    let tableBody = createElement("tbody", table, "");
    createTableRow(tableBody, "ISO4217", valuta.ISO4217)
    createTableRow(tableBody, "Creation Date", valuta.dateOfCreation.toDateString())
    createTableRow(tableBody, "Continent", valuta.mainContinent[1])
    createTableRow(tableBody, "Price Kg Gold", valuta.priceOfKiloGold)
}

const createCards = (valuta) => {
    if (valuta !== undefined) {

        if (valuta.length > 0) {
            valuta.forEach(v => createCard(v))
        } else {
            createCard(valuta);
        }
    }

}

function createTableRow(parent, title, value) {
    let tr = createElement("tr", parent, "");
    let th = createElement("th", tr, "");
    th.scope = "row";
    th.innerHTML = title;
    let td = createElement("td", tr, "")
    td.innerHTML = value;
}

const emptyCards = () => {
    ValutaCardGrid.innerHTML = '';
}

const displayErrorPage = () => {
    ValutaCardGrid.innerHTML =
        `
        <div class="d-flex align-items-center justify-content-center h-100">
            <div class="text-center row">
                <div class="col-md-6 align-self-center">
                    <h1 style="font-size: 10vw" class="text-capitalize text-warning">404</h1>
                </div>
                <div class=" col-md-6 mt-5">
                    <p class="fs-3"> <span class="text-danger">Whoopsie!</span> Data could not be retrieved.</p>
                    <p class="lead">
                        Please try again later
                    </p>
                    <button onClick="window.location.reload();"  class="btn btn-primary">Reload</button>
                </div>

            </div>
        </div>
        `
}
export {createCards, createElement, emptyCards, displayErrorPage}