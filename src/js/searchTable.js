import {createElement} from "./CardCreator.js";

const tbody = document.getElementById("searchTbody");

const fillSearchTable = (v) => {
    if (v.length > 0) {
        v.forEach(valuta => {
            let tr = createElement('tr', tbody, "search-table-cel")
            let th = createElement('th', tr, "")
            th.scope = "row";
            th.innerHTML = valuta.id;
            let ISO4217 = createElement('td', tr, "")
            ISO4217.innerHTML = valuta.ISO4217
            let name = createElement('td', tr, "")
            name.innerHTML = valuta.name
            let CreationDate = createElement('td', tr, "")
            CreationDate.innerHTML = valuta.dateOfCreation.toLocaleString().split(',')[0]
            let Continent = createElement('td', tr, "")
            Continent.innerHTML = valuta.mainContinent[1]
            let PriceKgGold = createElement('td', tr, "")
            PriceKgGold.innerHTML = valuta.priceOfKiloGold
        })
    }
}

const searchValutas = (valutas, filter) => {
    let result = valutas.filter(valuta => valuta.name.toLowerCase().includes(filter.toLowerCase())
        || valuta.ISO4217.toLowerCase().includes(filter.toLowerCase()))
    tbody.innerHTML = "";
    fillSearchTable(result);
}

export {searchValutas, fillSearchTable}