import {createValuta, deleteValuta, editValuta, getValutas} from "./db.js";
import {loadDetails, loadTable, refreshCards, reloadForm} from "./refreshFunctions.js";
import {popToast} from "./Toasts.js";
import {editFormToValuta, emptyNewForm, newFormToValuta} from "./form.js";
import {searchValutas} from "./searchTable.js";
import {updateBackground, updateFontSize} from "./settings.js";
import {navigateToDetail, navigateToEdit} from "./navigation.js";

const editButton = document.getElementById("EditValutaButton");
const deleteButton = document.getElementById("EditDeleteButton")
const createButton = document.getElementById("AddValutaButton")
const search = document.getElementById("Search")
const searchTableCells = document.getElementsByClassName("search-table-cel")
const detailToEditButton = document.getElementById("EditButton")

const addEditButtonEvent = () => {
    editButton.addEventListener('click',
        () => {
            try {
                editValuta(editFormToValuta()).then((v) => {
                    refreshCards();
                    loadDetails(v.id);
                    loadTable();
                    navigateToDetail();
                    popToast('Valuta updated', 'succes');
                }).catch(e => {
                    console.log(e);
                    popToast("Something went wrong", "red")
                })
            } catch (e) {
                popToast(e, "red")
            }
        })
}

const addDeleteButtonEvent = () => {
    deleteButton.addEventListener('click',
        () => {
        try{
            deleteValuta(editFormToValuta().id).then(() => {
                refreshCards()
                reloadForm();
                popToast("Valuta deleted!", "red")
            })} catch(e) {
                console.log(e);
                popToast("Something went wrong", "red")
            }
        }
    )
}

const addCreateValutaButton = () => {
    createButton.addEventListener('click', () => {
        try {
            createValuta(newFormToValuta()).then((v) => {
                refreshCards();
                emptyNewForm();
                reloadForm(v.id)
                loadDetails(v.id)
                navigateToDetail()
                popToast("Valuta created!", "green")
            }).catch(e => {
                console.log(e);
                popToast("Something went wrong", "red")
            })
        } catch (e) {
            popToast(e, 'red')
        }
    })
}

const AddSearchEvent = () => {
    search.addEventListener("input", () =>
        getValutas().then(v => {
            searchValutas(v, search.value);
            addTableCellEvents();
        }).catch(e => {
            console.log(e);
            popToast("Something went wrong", "red")
        }))
}

const addBackgroundSelectEvent = () => {
    let backgroundSelect = document.getElementById("BackgroundColorSelect")
    backgroundSelect.addEventListener("change", () => {
        updateBackground();
    })
}


const addFontSizeEvent = () => {
    let fontSizeSelect = document.getElementById("FontSizeSelect")
    fontSizeSelect.addEventListener("change", () => {
        updateFontSize();
    })
}

const addTableCellEvents = () => {
    for (let i = 0; i < searchTableCells.length; i++) {
        searchTableCells[i].addEventListener('click', () => {
            loadDetails(+searchTableCells[i].getElementsByTagName('th')[0].innerHTML)
            reloadForm(+searchTableCells[i].getElementsByTagName('th')[0].innerHTML)
            navigateToDetail();
        })
    }
}

const addDetailToEditButtonEvent = () => {
    detailToEditButton.addEventListener('click', () => {
        navigateToEdit();
    })
}

const addEventListeners = () => {
    addEditButtonEvent();
    addCreateValutaButton();
    addDeleteButtonEvent();
    AddSearchEvent();
    addBackgroundSelectEvent();
    addFontSizeEvent();
    addDetailToEditButtonEvent();
}

export {addEventListeners, addTableCellEvents}
