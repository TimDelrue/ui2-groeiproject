export const Continents = {
    EUROPE: "Europe",
    AFRICA: "Africa",
    ASIA: "Asia",
    NORTH_AMERICA: "North America",
    SOUTH_AMERICA: "South America",
    ANTARCTICA: "Antarctica",
    OCEANIA: "Oceania"
};

export default class Valuta {
    constructor(id, ISO4217, name, dateOfCreation, mainContinent, priceOfKiloGold, imageURL) {
        this.id = id;
        this.ISO4217 = ISO4217;
        this.name = name;
        this.dateOfCreation = dateOfCreation;
        this.mainContinent = mainContinent;
        this.priceOfKiloGold = priceOfKiloGold;
        this.imageURL = imageURL;
    }
}

const getValutaInstance = (obj) => {
    return new Valuta(obj.id, obj.ISO4217, obj.name, new Date(obj.dateOfCreation), stringToEnum(obj.mainContinent), obj.priceOfKiloGold, obj.imageURL)
}

const stringToEnum = (c) => {
    return Object.entries(Continents).find(t => (t[0]) === c)
}

const valutaToJsonString = (v) => {
    v.dateOfCreation = dateToString(new Date(v.dateOfCreation));
    return JSON.stringify(v);
}

const dateToString = (date) => {
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    return `${year}-${month}-${day}`
}

export {getValutaInstance, valutaToJsonString, dateToString}
