import {dateToString} from "./Valuta.js";
import {createElement} from "./CardCreator.js";

const detailImg = document.getElementById("DetailImg")
const detailCurrencyName = document.getElementById("DetailCurrencyName")
const detailISO4217 = document.getElementById("DetailCurrencyISO4217")
const detailCreationDate = document.getElementById("DetailCreationDate")
const detailContinent = document.getElementById("DetailContinent")
const detailPriceKgGold = document.getElementById("DetailPriceKgGold")
const countryTable = document.getElementById("DetailTableBody")
const detailTable = document.getElementById("detailTable");
const noCountriesDiv = document.getElementById("noCountriesDiv");


const fillDetailTable = (c) => {
    countryTable.innerHTML = "";
    if (c.length > 0) {
        detailTable.classList.remove("visually-hidden")
        noCountriesDiv.classList.add("visually-hidden")
        c.forEach(country => {
            let tr = createElement('tr', countryTable, "")
            let th = createElement('th', tr, "")
            th.scope = "row";
            th.innerHTML = country.name;

            let population = createElement('td', tr, "")
            population.innerHTML = country.population
            let isDemocratic = createElement('td', tr, "")
            isDemocratic.innerHTML = country.democratic
        })
    } else {
        detailTable.classList.add("visually-hidden")
        noCountriesDiv.classList.remove("visually-hidden")
    }

}

const fillDetailCard = (valuta) => {
    detailImg.src = valuta.imageURL;
    detailCurrencyName.innerHTML = valuta.name;
    detailISO4217.innerHTML = valuta.ISO4217;
    detailCreationDate.innerHTML = dateToString(valuta.dateOfCreation);
    detailContinent.innerHTML = valuta.mainContinent[1];
    detailPriceKgGold.innerHTML = valuta.priceOfKiloGold;
}

export {fillDetailCard, fillDetailTable}