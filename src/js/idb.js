import {openDB} from 'idb'
import {setBackGround, setFontSize} from './settings.js'

let db;

const startDB = async () => {
    db = await openDB('settingsDB', 1, {
        upgrade(db, oldVersion, newVersion, transaction, event) {
            const store = db.createObjectStore("settings", {keyPath: 'name'})
            store.createIndex('name', 'name')
        }
    })
    let settingsStore = db.transaction('settings', 'readwrite').objectStore('settings')
    let settings = await settingsStore.getAll()
    if (!settings.length) await addSettings();
}

const readSettings = async () => {
    let settingStore = db.transaction('settings').objectStore('settings')
    let settings = await settingStore.getAll()
    let backgroundColor = settings.find(b => b.name === "background")
    let fontSize = settings.find(b => b.name === "fontSize")
    setBackGround(backgroundColor.value)
    setFontSize(fontSize.value)
}

const addSettings = async () => {
    await AddSetting('background', 'bg-light');
    await AddSetting('fontSize', 'fs-1');
}

const AddSetting = async (name, value) => {
    let tx = db.transaction('settings', 'readwrite');
    try {
        await tx.objectStore('settings').add({name, value})
    } catch (err) {
        console.log(err)
    }
}

const updateSettings = async (name, value) => {
    let tx = db.transaction('settings', 'readwrite');
    try {
        await tx.objectStore('settings').put({name, value})

    } catch (err) {
        console.log(err)
    }
}

export {startDB, readSettings, updateSettings}