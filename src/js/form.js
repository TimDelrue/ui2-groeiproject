const editId = document.getElementById('EditId')
const editName = document.getElementById('EditValutaName')
const editISO4217 = document.getElementById('EditValutaISO4217')
const editDateOfCreation = document.getElementById('EditValutaDateOfCreation')
const editContinent = document.getElementById('EditValutaContinent')
const editPriceKgGold = document.getElementById('EditValutaPriceKgGold')
const editImgUrl = document.getElementById("EditImageUrl");

const newName = document.getElementById('AddValutaName')
const newISO4217 = document.getElementById('AddValutaISO4217')
const newDateOfCreation = document.getElementById('AddValutaDateOfCreation')
const newContinent = document.getElementById('AddValutaContinent')
const newPriceKgGold = document.getElementById('AddValutaPriceKgGold')
const newImgUrl = document.getElementById('AddValutaImageUrl')

const emptyNewForm = () => {
    newName.value = ""
    newISO4217.value = ""
    newDateOfCreation.value = null
    newContinent.value = ""
    newPriceKgGold.value = null
    newImgUrl.value = ""
}

const fillEditForm = (valuta) => {
    editId.innerHTML = 'Id : ' + valuta.id;
    editId.classList.add("visually-hidden");
    editName.value = valuta.name;
    editISO4217.value = valuta.ISO4217;
    editDateOfCreation.value = valuta.dateOfCreation.toISOString().substring(0, 10);
    editContinent.value = valuta.mainContinent[0];
    editPriceKgGold.value = valuta.priceOfKiloGold;
    editImgUrl.value = valuta.imageURL;
}

const getNumberFromString = (o) => {
    var numb = o.match(/\d/g);
    numb = numb.join("");
    return numb;
}

const editFormToValuta = () => {
    let v = {};
    let errors = []
    v.id = getNumberFromString(editId.innerHTML)
    if (editName.value) v.name = editName.value; else errors.push("Name can't be empty")
    if (editISO4217.value) v.ISO4217 = editISO4217.value; else errors.push("ISO4217 can't be empty")
    if (editDateOfCreation.value) v.dateOfCreation = editDateOfCreation.value; else errors.push("Date of creation can't be emtpy")
    if (editContinent.value) v.mainContinent = editContinent.value; else errors.push("Continent can't be empty")
    if (editPriceKgGold.value) {
        if (editPriceKgGold.value < 0) errors.push("Price kg gold can't be negative")
        else v.priceOfKiloGold = editPriceKgGold.value;
    } else errors.push("Price per kg gold can't be empty")
    v.imageURL = editImgUrl.value;

    if (errors.length > 0) {
        throw errors.join("<br>")
    }
    return v;
}

const newFormToValuta = () => {
    let v = {};
    v.id = 0;
    let errors = [];
    if (newISO4217.value) v.ISO4217 = newISO4217.value; else errors.push("ISO4217 is empty, please give it a value.")
    if (newName.value) v.name = newName.value; else errors.push("Name is empty, please give it a value.")
    if (newDateOfCreation.value) v.dateOfCreation = newDateOfCreation.value; else errors.push("Date of creation is empty, please give it a value.")
    if (newContinent.value) v.mainContinent = newContinent.value; else errors.push("Continent is empty, please give it a value.")
    if (newPriceKgGold.value) {
        if (newPriceKgGold.value < 0) errors.push("Price per kilo gold can not be negative, please give it a different value.")
        else v.priceOfKiloGold = +newPriceKgGold.value;
    } else errors.push("Price per kilo gold is empty, please give it a value.")
    v.imageURL = newImgUrl.imageURL ? newImgUrl.value : "https://i.pinimg.com/originals/94/8c/36/948c367d34fca94e28c3b7970f9fbefb.jpg"
    if (errors.length > 0) {
        throw errors.join("<br>");
    }
    return (v);
}

export {fillEditForm, editFormToValuta, newFormToValuta, emptyNewForm}