import {updateSettings} from "./idb.js";

const updateBackground = () => {
    let backgroundSelect = document.getElementById("BackgroundColorSelect");

    setBackGround(backgroundSelect.value)

}

const updateFontSize = () => {
    let FontSizeSelect = document.getElementById("FontSizeSelect");

    setFontSize(FontSizeSelect.value);

    updateSettings('fontSize', FontSizeSelect.value).then();
}
const setFontSize = (clas) => {
    let FontSizeSelect = document.getElementById("FontSizeSelect");

    let body = document.querySelector("body");

    body.classList.remove("fs-6")
    body.classList.remove("fs-5")
    body.classList.remove("fs-3")
    body.classList.remove("fs-1")
    body.classList.add(clas);


    updateSettings('fontSize', clas).then(FontSizeSelect.value = clas);
}

const setBackGround = (clas) => {
    let backgroundSelect = document.getElementById("BackgroundColorSelect");

    let body = document.querySelector("body");

    body.classList.remove("bg-blue")
    body.classList.remove("bg-secondary")
    body.classList.remove("bg-success")
    body.classList.add(clas);

    updateSettings('background', clas).then(backgroundSelect.value = clas)
}

export {updateBackground, updateFontSize, setBackGround, setFontSize}