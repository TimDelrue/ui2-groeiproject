const navigateToDetail = () => {
    document.getElementsByClassName('pane-locator active show')[0].classList.remove('active', 'show');
    document.getElementsByClassName('pill-locator active')[0].classList.remove('active')

    document.getElementById("nav-detail").classList.add('active', 'show')
    document.getElementById("nav-detail-tab").classList.add('active')
}

const navigateToEdit = () => {
    document.getElementsByClassName('pane-locator active show')[0].classList.remove('active', 'show');
    document.getElementsByClassName('pill-locator active')[0].classList.remove('active')

    document.getElementById("nav-edit").classList.add('active', 'show')
    document.getElementById("nav-edit-tab").classList.add('active')
}

export {navigateToEdit, navigateToDetail}