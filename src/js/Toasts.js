const alert = document.getElementById("alert");
const livetoast = document.getElementById("livetoast");
import * as bootstrap from 'bootstrap';

const popToast = (text, type = "normal") => {
    alert.innerHTML = text;
    let toast = new bootstrap.Toast(livetoast);
    switch (type) {
        case "succes":
        case "green":
        case "good":
            livetoast.className = 'toast bg-success'
            break;
        case "fail":
        case "error":
        case "bad":
        case "red":
            livetoast.className = 'toast bg-danger'
            break;
        case "normal":
        case "default":
        default:
            livetoast.className = 'toast';
            break;
    }
    toast.show()
}

export {popToast}