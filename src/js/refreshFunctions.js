import {createCards, displayErrorPage, emptyCards} from "./CardCreator.js";
import {getCountriesFromValuta, getFirstValuta, getValutaById, getValutas} from "./db.js";
import {fillEditForm} from "./form.js";
import {fillSearchTable} from "./searchTable.js";
import {readSettings} from "./idb.js";
import {fillDetailCard, fillDetailTable} from "./detail.js";
import {addTableCellEvents} from "./eventListeners.js";

const refreshCards = () => {
    emptyCards();
    getValutas().then(v => createCards(v)).catch(e => console.log(e))
}

const refreshForm = () => {
    getFirstValuta().then(v => fillEditForm(v)).catch(e => {
        console.log("ERROR: could not refresh from: \n");
        console.log(e);
        displayErrorPage();
    });
}

const reloadForm = (id) => {
    if (id) {
        getValutaById(id).then(v => fillEditForm(v)).catch(e => console.log(e))
    } else {
        getFirstValuta().then(v => fillEditForm(v)).catch(e => console.log(e));
    }
}
const loadTable = () => {
    getValutas().then(v => fillSearchTable(v)).catch((e) => console.log(e)).then(addTableCellEvents);
}

const loadSettings = () => {
    readSettings().then().catch(e => console.log(e))
}

const loadDetails = (id) => {
    if (id) {
        getValutaById(id).then(v => {
            fillDetailCard(v);
            getCountriesFromValuta(v).then(c => fillDetailTable(c)).catch(e => console.log(e));
        }).catch(e => console.log(e))
    } else {
        getFirstValuta().then(v => {
            fillDetailCard(v);
            getCountriesFromValuta(v).then(c => fillDetailTable(c)).catch(e => console.log(e));
        }).catch(e => console.log(e))
    }
}

export {refreshCards, reloadForm, loadTable, readSettings, loadSettings, refreshForm, loadDetails}