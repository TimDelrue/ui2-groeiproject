import {getValutaInstance, valutaToJsonString} from './Valuta.js';
import {popToast} from "./Toasts.js";
import {getCountryInstance} from "./country.js";
import {loadDetails} from "./refreshFunctions.js";

const dbURL = 'http://localhost:3000/'

const getFirstValuta = async () => {
    try {
        let respon = await fetch(dbURL + 'valutas');
        let data = await respon.json();
        return getValutaInstance(data[0])
    } catch (e) {
        console.log("ERROR: Failed to first valuta.\n");
        throw e
    }
}

const getValutas = async () => {
    try {
        let respon = await fetch(dbURL + 'valutas')
        let data = await respon.json()
        const valutaArray = []
        data.forEach(valuta => valutaArray.push(getValutaInstance(valuta)))
        return valutaArray
    } catch (e) {
        console.log("ERROR: Failed to get valutas.\n");
        throw e
    }
}

const editValuta = async (valuta) => {
    let json = valutaToJsonString(valuta);
    try {
        let respon = await fetch(dbURL + 'valutas/' + valuta.id, {
            method: 'PUT',
            headers: {
                'Content-Type': "application/json",
            },
            body: json
        })
        popToast("Valuta updated!", "green");
        loadDetails(+valuta.id)
        return respon.json()
    } catch (e) {
        console.log("ERROR: Failed to update valuta {" + valuta.toString() + "}.\n");
        throw e
    }

}

const deleteValuta = async (id) => {
    try {
        let respon = await fetch(dbURL + "valutas/" + id, {
            method: 'DELETE'
        })
        return respon.json();
    } catch (e) {
        console.log("ERROR: Failed to get valuta with id: " + id + ".\n");
        throw e
    }

}

const createValuta = async (o) => {
    let json = valutaToJsonString(o)
    try {
        let respon = await fetch(dbURL + 'valutas/', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: json
        });
        return respon.json();
    } catch (e) {
        console.log("ERROR: Failed to add valuta {" + o.toString() + "}.\n");
        throw e
    }

}

const getCountriesFromValuta = async (valuta) => {
    try {
        let respon = await fetch(dbURL + "countries/")
        let data = await respon.json();
        const array = []
        data.forEach(country => {
            if (country.currencyId === valuta.id) {
                array.push(getCountryInstance(country))
            }
        })
        return array;
    } catch (e) {
        console.log("ERROR: Failed to get countries.\n");
        throw e
    }

}

const getValutaById = async (id) => {
    try {
        let respon = await fetch(dbURL + "valutas/")
        let data = await respon.json()
        return getValutaInstance(data.find(v => v.id === id))
    } catch (e) {
        console.log("ERROR: Failed to get valuta by id: " + id + ".\n");
        throw e
    }

}

export {getFirstValuta, getValutas, editValuta, deleteValuta, createValuta, getCountriesFromValuta, getValutaById}

/*
=== Week 1 + 2 ===

export function getValutas(){
    let valutaArray = [];


    valutaArray.push(new Valuta(1,"EUR","Euro",new Date(1991,1,1),v.Continents.Europe, 55931,"https://cdn.icon-icons.com/icons2/1369/PNG/512/-euro-symbol_90430.png"));
    valutaArray.push(new Valuta(2,"USD","US Dollar",new Date(1785,7,6),v.Continents.NorthAmerica, 60000,"https://cdn.icon-icons.com/icons2/510/PNG/512/social-usd_icon-icons.com_49994.png"));
    valutaArray.push(new Valuta(3,"AUD","Australian Dollar",new Date(1966,2,14),v.Continents.Oceania, 86921,"https://static.vecteezy.com/system/resources/previews/010/653/308/original/australia-currency-aud-australian-dollar-icon-symbol-illustration-vector.jpg  "));
    valutaArray.push(new Valuta(4,"GBP","Pound Sterling",new Date(1489,1,1),v.Continents.Europe, 49673,"https://cdn.icon-icons.com/icons2/1960/PNG/512/sterlingpoundsymbol_122810.png"));

    console.log(valutaArray);

    return valutaArray;
}

export async function editValuta(v){
    let json = await valu.ToJsonString(v)
    let respon = await fetch(reguestUrl + "valuta/" + v.id, {

    })
}
*/
